// **********************************************************************
// **** This script adds a <div> element to the DOM with and id='overlay'
// **********************************************************************

// Wait for the DOM to load(8 seconds)...
// Then grab the .gm-style div element....
// and create overlay div element, add id to it...
// Finally, inseret the newly created overlay div before the firstChild of
// .gm-style element
setTimeout(function () {
  var overlayContainer = document.querySelector('.gm-style');
  var overlay = document.createElement('div');
  overlay.id = 'overlay';
  overlayContainer.style.height = '100%';
  overlayContainer.insertBefore(overlay, overlayContainer.firstChild);
}, 800);
