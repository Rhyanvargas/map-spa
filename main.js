
(function initMap() {
  //************************* GET DOM INFO **********************************
  var mapDiv = document.getElementById('map');
  var modalOpen = document.getElementsByClassName('modal-open');
  var myModal = document.getElementById('myModal');
  var myModalCloseIcon = document.getElementById('myModalCloseIcon');
  var myModalImage = document.getElementById("myModalImage");
  var myModalLabel = document.getElementById("myModalLabel");
  var myModalBody = document.getElementById("myModalBody");
  var imageURL = myModalImage.getAttribute("src");
  var iconPathActive = './assets/images/pin-hover.png';
  var iconPath = './assets/images/pin.png';
  var overlaySrc = './assets/images/map-overlay.png';
  //************************* SET MAP STYLES *********************************
  var styles = [
    {
      default:null,
      featureType:'poi.medical',
      stylers:[{visibility: 'off'}]
    },
    {
      featureType:'poi.school',
      stylers:[{visibility: 'off'}]
    },
    {
      featureType:'poi.government',
      stylers:[{visibility: 'off'}]
    },
    {
      featureType:'poi.place_of_worship',
      stylers:[{visibility: 'off'}]
    }
  ];
  //************************* SET MAP OPTIONS ********************************
  var options = {
    zoom:14,
    center:{lat:48.8566,lng:2.3522},
    scrollwheel:false,
    zoomControl:false,
    panControl:false,
    streetViewControl:false,
    fullscreenControlOptions:false,
    gestureHandling:"none",
    mapTypeControl:false,
    mapTypeId:'hybrid',
    styles: styles
  };
  //************************* NEW MAP ***************************************
  var map = new google.maps.Map(mapDiv, options);
  //***** MARKERS and PLACES ARRAY *****
  var markers = [];
  var places = [
    {
      label:'Seeing is Believing',
      image:'./assets/images/img-for-blade-0.png',
      body:'THE LOURVE, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      coords:{lat:48.8606,lng:2.337600000000066}
    },{
      label:'Bataclan',
      image:'./assets/images/img-for-blade-1.png',
      body:'BATACLAN consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      coords:{lat:48.863,lng:2.3705999999999676}
    },{
      label:'The Seine River',
      image:'./assets/images/img-for-blade-2.png',
      body:'SIENE, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      coords:{lat:48.8508217,lng:2.3477781000000277}
    }
  ];
  //************************ RESET ICON *************************************
  var resetIcon = function(){
    for(var i = 0; i < markers.length; i++) {
      markers[i].setIcon(iconPath);
    };
  };
  //************************ ADD MARKERS ON MAP *****************************
  for(i = 0; i < places.length;i++){
    // initialize place variables
    var currentPlace = places[i];
    var latlng = currentPlace.coords;
    var image = currentPlace.image;
    var label = currentPlace.label;
    var body = currentPlace.body;
    // make new marker
    var marker = new google.maps.Marker({
      position:latlng,
      map:map,
      icon:iconPath
    });
    // push marker to array
    markers.push(marker);

    //***************** ADD CLICK LISTENERS TO EACH MARKER ****************
    var showModal = function(marker,currentPlace) {
      marker.addListener('click', function(){
        // remove yellow icons that are not active
        for(var j = 0; j < markers.length; j++) {
          markers[j].setIcon(iconPath);
        };
        // set icon to active icon image onclick
        marker.setIcon(iconPathActive);
        // insert the modal content
        myModalImage.setAttribute('src', currentPlace.image);
        myModalLabel.innerText = currentPlace.label;
        myModalBody.innerText = currentPlace.body;
        // show modal
        $('#myModal').modal('show');
      });
    };
    // show the modal based on the marker clicked
    showModal(marker,currentPlace);
  };
  //******************** RESET ICONS TO ORIGINAL STATE **********************
  $('#myModal').on('click', function(){
    resetIcon();
  });
  $('#myModalCloseIcon').on('click',function() {
    resetIcon();
  });
  //*************** AUTO-CENTER MAP WHEN RESIZING WINDOW ********************
  google.maps.event.addDomListener(window, "resize", function() {
    var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center);
  });

})(); // END OF INIT MAP
