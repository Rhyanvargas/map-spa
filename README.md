# Google Map SPA #

* [VIEW DEMO HERE](https://map-spa.rhyanvargas.com)

### What is this repository for? ###

* To showcase my front-end skills by building a Map SPA using Google Maps API, HTML, CSS, Javascript, jQuery and Bootstrap
* Version 1.0

### How do I get set up? ###

1. `git clone` this repository
2. Open index.html in a browser

* Dependencies
  - none

### Post-project Thoughts ###

* Bootstrap source Fallback [reference](https://stackoverflow.com/a/26198380)
* Using Props
  - I could have used `props.coords` or `props.iconImage` to pass in different properties in the `addMarker()` function...but since all markers used the same icon image, it would not make sense to do this, if we are not worry about scalabitliy of adding different marker typs. With that said, using `props` would look like:
  ```
  // Add Marker 1
  addMarker({
    coords:{lat:48.8606,lng:2.3376},
    iconImage:iconPath 
  });
  // Add Marker 2
  addMarker({
    coords:{lat:48.75347,lng:2.7345},
    iconImage:iconPath 
  });

  // Marker Function
  addMarker(props){
    var marker = new google.maps.Marker({
      position:props.coords,
      icon:props.iconImage,
      map:map
    });
  };
  ```

### Future Plans for this Project ###

* Re-build this with React
* Make this into a Progressive Web App (React's new `create-react-app` sets up a PWA by default)
